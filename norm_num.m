function v = norm_num(G, p, varargin)
%NORM_NUM   Numerically computes the inf or 2 norm of a linear system for a given frequency grid
%
%   G   - System on State space form, transfer function form or frequency
%   response
%
%   p   -   2   -- 2 Norm
%           inf -- infinity norm

w = [];
G_fr = [];

if isnumeric(G)
    G_fr = G;
    if numel(varargin) >= 1
        w = varargin{1};
    else
        error('Must supply frequency vector with numeric frequency response')
    end
elseif numel(varargin) >= 1
    w = varargin{1};
    G_fr = squeeze(freqresp(G, w));
else
    G_fr, w = squeeze(freqresp(G, w));
end


if p==2
    symmetry_factor = 2;
    if min(w) < 0        
        symmetry_factor = 1;
    end
    
    v = sqrt(symmetry_factor * trapz(w, abs(G_fr).^2) /2/pi);
elseif p==inf
    v = max(abs(G_fr));
else
    error('Norm not supported')
end
