%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%         Example 2          %%%%%%%%%%%
%%%%%%%%%%%        Surpass LQG         %%%%%%%%%%%
%%%%%%%%%%%  Single Pole - Time Delay  %%%%%%%%%%%
%%%%%%%%%%% Written by Olle Kjellqvist %%%%%%%%%%%
%%%%%%%%%%%         2018/07/19         %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% NOTE:
% You can play around with the order of the Padé-approximant. Note the
% difference in oscillatory behaviour between Q-design and LQG-design 
% using a Padé-approximant when evaluating the gang of four.
%% Define problem
s = tf('s');
P = ss(exp(-s)/(1 + s));        % Process
D_dist = ss(1/(1 + s*10));      % Load disturbance spectral factor
W_proc = 200;                   % Load disturbance intensity
W_sensor = 0.001;               % Sensor noise (white) intensity
rho = .01;                       % Weight

% LQG controller can't handle time delay, so get an Padé approximant
P_pade = ss(pade(exp(-P.outputdelay*s), 2) * P);
if P_pade.internaldelay ~= 0
    P_pade.internaldelay = 0;
end

% Controller to recover
C_lqg = lqg_design(P_pade, D_dist, W_proc, W_sensor, rho);

%% Generate a Ritz expansion of Laguerre filters
a = 23;         % Cut-off frequency
N = 10;        % Number of elements in the basis expansion

% This is a little bit of magic to generate the state-space matrices
D = kron([-a -2*a*(-1).^(1:(N-1))],ones(N, 1));
A = full(spdiags(D, 0:N-1, N, N));
B = eye(N);
C = sqrt(2*a)*(-1).^(0:N-1);
Q = ss(A, B, C, 0);

%% Set up optimization
% Get cost matrices
[Cquad, Clin, Cconst] = genQPdelay(P, D_dist, W_proc, W_sensor, rho, Q);

Alin = zeros(size(Cquad));

[res] = mskqpopt(2*Cquad, Clin, Alin)

b = res.sol.itr.xx;

% Calculate cost and maximum sensitivity
b = res.sol.itr.xx(1:N);
cost = res.sol.itr.pobjval + Cconst;

%% Present
w_dense = logspace(-8,6,25000)';
% Plot using P with time-delay
gangof4_plot(P, C_lqg, w_dense)
gangof4_plot_Q(P, Q*b, w_dense)
legend('C_{lqg}', 'Q')
%%
% Numerically evaluate the cost. Note that this is quite difficult and
% requires a really dense grid.
J_lqg_num = norm_num(squeeze(freqresp(feedback(P, C_lqg)*D_dist, w_dense)),2,w_dense)^2*W_proc^2 + norm_num(squeeze(freqresp(feedback(C_lqg*P, 1),w_dense)),2,w_dense)^2*W_sensor^2 + ...
rho* (norm_num(squeeze(freqresp(feedback(P*C_lqg, 1)*D_dist,w_dense)),2,w_dense)^2*W_proc^2 + norm_num(squeeze(freqresp(feedback(C_lqg, P),w_dense)),2,w_dense)^2*W_sensor^2);

fprintf('\nCost LQG, (evaluated numerically)  %5d\n', J_lqg_num)
fprintf('Cost MOSEK                         %5d\n', cost)