function gangof4_plot(P, C, w, varargin)
%GANGOF4_PLOT Plots the Gang of four, only SISO systems are supported
%
%   gangof4plot(P, C, w)
%   P   - Plant: SS form, TF form or frequency response are supported.
%   C   - Controller: SS form, TF form or frequency response are supported.
%   w   - frequency grid
%
%   Optional:
%   gangof4plot(P, C, w, freq_label)
%   freq_label = 'Hz' , plots in Hz


% Check if the response should be in Hertz or rad/s
freq_label = [];
if numel(varargin) >= 1 && strcmp(varargin(1), 'Hz')
    x = w/2/pi;
    freq_label = 'Frequency [Hz]';
else
    x = w;
    freq_label = 'Frequency [rad/s]';
end




% Check if the transfer functions are provided as linear system objects,
% or numiercally as frequency responses
P_fr = [];
if isnumeric(P)
    P_fr = P;
else
    P_fr = squeeze(freqresp(P, w));
end

C_fr = [];
if isempty(C)
    return
elseif isnumeric(C)
    C_fr = C;
else
    C_fr = squeeze(freqresp(C, w));
end

S_fr = 1./(1+P_fr.*C_fr);


apply_common_style = @() evalin('caller', 'xlabel(freq_label); set(gca, ''XLim'', [w(1) w(end)]); grid on; hold on;');


subplot(221)
loglog(x, abs(S_fr))
title('S = 1/(1+PC)')
apply_common_style()

subplot(222)
loglog(x, abs(P_fr .* S_fr))
hold on
%loglog(x, abs(squeeze(freqresp(G_Cav,w))), [Ppc '--'])
apply_common_style()
title('P/(1+PC)')

subplot(223)
loglog(x, abs(C_fr .* S_fr))
title('C/(1+PC)')
apply_common_style()

subplot(224)
loglog(x, abs(C_fr .* P_fr .* S_fr))
title('PC/(1+PC)')
apply_common_style()
end