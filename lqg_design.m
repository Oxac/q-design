function C_lqg = lqg_design(P, D_dist, W_proc, W_sensor, rho)
%LQG_DESIGN  Simple LQG design for SISO systems,
%   only considers one disturbance input with white, or colored noise.
%
%   C_lqg = lqg_design(P, D_dist, W_proc, W_sensor, rho)
%
%   P           - system model on state-space form
%   D_dist      - Disturbance model on state-space form, D_dist = ss(1) <-> white noise
%   W_proc      - Intensity of the disturbance white noise
%   W_sensor    - Sensor noise covariance matrix.
%   rho         - J_uu where J = Integral{x'Qx + u'(rho)u}

[A, B, C, D] = ssdata(P);

[Aw, Bw, Cw, Dw] = ssdata(D_dist);


if hasdelay(P)
    warning('Cannot design LQG controller for system with time-delay.')
    C_lqg = ss(0);
    return
end

if isempty(Aw)
    Ae = A;
    Be = [B B];
    Ce = C;
else
    Ae = [A, B*Cw; 0*Bw*C Aw]; 
    Be = [[B; Bw*0] [B*0; Bw]];
    Ce = [C 0*Cw];
end


[K,S]=lqr(Ae,Be(:,1),Ce'*Ce,rho);

G = B;
H = 0*C*B;
[kest,L]=kalman(ss(Ae,Be,Ce,[D H]), W_proc^2, W_sensor^2);
C_lqg = ss(Ae-L*Ce-Be(:,1)*K,L,K,0*K*L);