%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%         Example 1          %%%%%%%%%%%
%%%%%%%%%%%        Recover LQG         %%%%%%%%%%%
%%%%%%%%%%%  Single Pole - Time Delay  %%%%%%%%%%%
%%%%%%%%%%% Written by Olle Kjellqvist %%%%%%%%%%%
%%%%%%%%%%%         2018/07/19         %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Define problem
s = tf('s');
P = ss(exp(-s)/(1 + s));        % Process
D_dist = ss(1/(1 + s*10));      % Load disturbance spectral factor
W_proc = 200;                   % Load disturbance intensity
W_sensor = 0.001;               % Sensor noise (white) intensity
rho = .3;                       % Weight

% LQG controller can't handle time delay, so get an Padé approximant
P = ss(pade(exp(-P.outputdelay*s), 1) * P);
if P.internaldelay ~= 0
    P.internaldelay = 0;
end

% Controller to recover
C_lqg = lqg_design(P, D_dist, W_proc, W_sensor, rho);
% Cost of using said controller
Cost_lqg = norm(feedback(P, C_lqg)*D_dist)^2*W_proc^2 + norm(feedback(C_lqg*P, 1))^2*W_sensor^2 + ...
    rho* (norm(feedback(P*C_lqg, 1)*D_dist)^2*W_proc^2 + norm(feedback(C_lqg, P))^2*W_sensor^2);

% Maximum sensitivity
S_lqg = feedback(1,C_lqg*P);
MS_lqg = norm(S_lqg, inf)

%% Generate a Ritz expansion of Laguerre filters
a = 23;         % Cut-off frequency
N = 100;        % Number of elements in the basis expansion

% This is a little bit of magic to generate the state-space matrices
D = kron([-a -2*a*(-1).^(1:(N-1))],ones(N, 1));
A = full(spdiags(D, 0:N-1, N, N));
B = eye(N);
C = sqrt(2*a)*(-1).^(0:N-1);
Q = ss(A, B, C, 0);

%% Set up optimization
% Get cost matrices
[Cquad, Clin, Cconst] = genQPdelay(P, D_dist, W_proc, W_sensor, rho, Q);

Alin = zeros(size(Cquad));

[res] = mskqpopt(2*Cquad, Clin, Alin)

b = res.sol.itr.xx;

% Calculate cost and maximum sensitivity
b = res.sol.itr.xx(1:N);

A_T = [P.a P.b*C
    zeros(N, size(P.a,2)) A];
B_T = [zeros(size(P.a,1),1)
    b];
C_T = [P.c zeros(1, size(A,1))];

%% Evaluate
S   = 1 - ss(A_T, B_T, C_T, 0);
MS  = norm(S, inf);
cost = res.sol.itr.pobjval + Cconst;

%% Present
fprintf('\nThe cost using LQG is:         %5d \n', Cost_lqg)
fprintf('The cost using Q_design is:    %5d \n', cost)
fprintf('MS using LQG is:               %5d \n', MS_lqg)
fprintf('MS using LQG is:               %5d \n', MS)

w_dense = logspace(-8,6,10000)';
gangof4_plot(P, C_lqg, w_dense)
gangof4_plot_Q(P, Q*b, w_dense)