%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%          Example 3           %%%%%%%%%%
%%%%%%%%%%       Mixed H_2/H_inf        %%%%%%%%%%
%%%%%%%%%%   Damage-mitigation testbed  %%%%%%%%%%
%%%%%%%%%% Originally from Sznaier_2000 %%%%%%%%%%
%%%%%%%%%%  Written by Olle Kjellqvist  %%%%%%%%%%
%%%%%%%%%%          2018/07/20          %%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all
%% Define problem parameters
%
% Plant on state-space form
%
Ap = [-0.2185, 110.2712, -1.0704,  -0.4213, -1.1968, -0.0013, -0.0777
    -110.2801, -0.9896,  3.7338,   2.1488,  1.8831, -0.0787,  0.1621
      -1.4114, -4.5158, -12.9851,  42.7061, -41.2727, -1.4982, -2.1739
      -0.8933, -2.5601, -51.8080,  -5.2078, -82.6450, 0.5258, 3.2744
       1.4331,  2.2632,  48.7714,  82.7986,  -9.4665, 2.5266, -1.1314
       0.1010,  0.2228,  2.1113,   0.8446,  -2.8446,  -0.1284, -145.9587
       -0.2674, -0.6012, -3.4584,  -4.3014, 4.8333,   145.9948, -0.9545];

Bp = [2.7114, 5.7531, 10.4115, 6.1052, -7.5692, -0.6268, 1.7047]';

Cp = [0.5669, -0.0827, 7.2570, -3.8842, -3.4930, 0.6009, 1.6802
     2.6546, -5.7525, 7.4713, 4.7102,  6.7150,  -0.1782, 0.2881];
D = [0;0];

P = ss(round(Ap,2), round(Bp,2), round(Cp,2), 0);

%
% Filters
%
s = tf('s');
% Notch filter
W1 = (1.1531*s + 5.6566)/(0.001*s^2 + 0.0091*s + 11.384);
% Low-pass filter
W2 = ((10^(-4)*s + 1)/(0.1*s + 1))^2;

P2W1 = balreal(minreal(P(2)*W1));
P1W2 = balreal(minreal(P(1)*W2));

%% Set up optimization matrices
gamma = .0051;      % Weighted sensitivity bound
N = 200;            % Number of elements in the Ritz expansion
a = 100;            % Cut-off frequency of the Laguerre filters
Nw = 5000;          % Number of points in the frequency grid

% Construct Q such that Q*b is the Ritz expansion of the Youla-parameter
D_q = kron([-a -2*a*(-1).^(1:(N-1))],ones(N, 1));
A_q = full(spdiags(D_q, 0:N-1, N, N));
B_q = eye(N);
C_q = sqrt(2*a)*(-1).^(0:N-1);
Q = ss(A_q, B_q, C_q, 0);

% eta_2 on state-space form
A2 = [P2W1.a P2W1.b*C_q
     zeros(N, size(P2W1.a,2)) A_q];
B2 = [zeros(size(P2W1.a,1),N)
        B_q];
C2 = [P2W1.c zeros(1,N)];

%
% Quadratic cost
% Note the small regulation term. It's kept to make sure that the matrix is
% Positive definite.
%
Cquad = B2'*lyap(A2', C2'*C2)*B2 + 5e-6*eye(N);     
F = chol(Cquad);                                    % F'*F = Q, for cone form

% eta_inf on state-space form
Ainf = [P1W2.a P1W2.b*C_q
     zeros(N, size(P1W2.a,2)) A_q];
Binf = [zeros(size(P1W2.a,1),N)
        B_q];
Cinf = [P1W2.c zeros(1,N)];

Tinf = ss(Ainf, Binf, Cinf, 0);     % State-space object of eta_inf

%
% Get matrices for linear constraints
%
w = logspace(-2,3, Nw)';                % Frequency grid

Tinf_fr = squeeze(freqresp(Tinf,w));
W2_fr = squeeze(freqresp(W2, w));

C_re = real(Tinf_fr)';                  % Real part - goes into A-matrix
C_im = imag(Tinf_fr)';                  % Imaginary part - goes into A-matrix
b_re = real(W2_fr);                     % Real part of lower bound
b_im = imag(W2_fr);                     % Imaginary part of lower bound

%
% Set up Mosek
clear prob;
clear i_h_infty;

[r, res] = mosekopt('symbcon');

%
% Set up linear part
%

% Linear Cost
prob.c      = [zeros(1,N) zeros(1,N + 2*Nw) 1 0 zeros(1, Nw)];

% Linear inequality constraints
prob.a      = [F, -speye(N), sparse([],[],[],N, Nw), sparse([], [], [], N, Nw), sparse([], [], [], N,Nw + 2)
               C_re, sparse([], [], [], Nw, N), speye(Nw), sparse([], [], [], Nw, Nw), sparse([], [], [], Nw,Nw + 2)
               C_im, sparse([], [], [], Nw, N), sparse([], [], [], Nw, Nw), speye(Nw), sparse([], [], [], Nw,Nw + 2)];
prob.blc    = [sparse([],[],[],N,1)
                b_re
                b_im]; % Lower bound
prob.buc    = [sparse([],[],[],N,1)
                b_re
                b_im]; % Upper bound
%
% Set up variable bounds
% 
prob.blx    = [-inf*ones(1, 2*N+2*Nw + 1) 1/2 -inf*ones(1,Nw)]';
prob.bux    = [inf*ones(1, 2*N+2*Nw + 1) 1/2 gamma*ones(1, Nw)]';


%
% Set up conic part:
% One rotated second-order cone for quadratic cost
% Nw second-order cones for H_infty constraints
% 
prob.cones.type     = [res.symbcon.MSK_CT_RQUAD ones(1, Nw)*res.symbcon.MSK_CT_QUAD];

i_re    = (2*N+1):(2*N + Nw);           % Indices for real parts
i_im    = (2*N + Nw + 1):(2*N + 2*Nw);  % Indices for imaginary parts
i_bound = (2*N + 2*Nw + 2) + (1:Nw);    % Indices for bounds

%
% Put all indices into one vector
% 
i_h_infty(1:3:3*Nw) = i_bound;
i_h_infty((1:3:3*Nw) + 1) = i_re;
i_h_infty((1:3:3*Nw) + 2) = i_im;
% 
% Specify which cones are built from which variablesx
% 
prob.cones.sub      = [(2*N + 2*Nw + 1) (2*N + 2*Nw+2) (N+1):(2*N) i_h_infty];
% H_infty starts at N+3
prob.cones.subptr    = [1 N+3:3:(N+2+3*Nw)];
% Run Mosek
[r, res]    = mosekopt('minimize echo(9)', prob);

% Display primal solution for the variables.
b = res.sol.itr.xx(1:N);
% And cost
cost_MOSEK = sqrt(res.sol.itr.pobjval)

%% Evaluate in frequency domain
%
% Sparser grid
% 
w_dense = logspace(-4, 6, 10000)';

figure(4)
bode(P2W1*Q*b)
title('xi2')

figure(5)
bode(W2 - P1W2*Q*b, w_dense)
title('xi_{\infty}')

cost_norm = norm(P2W1*Q*b,2);
bound_norm = norm(W2 - P1W2*Q*b,'inf');

%% Simulate responses to a triangle wave input
t = 0:.002:1.4;
f = 5.84;
x = .5*(-1+sawtooth(1-2*pi*f*t,.5));
y1 = lsim(P(1)*Q*b, x, t);
y2 = lsim(P(2)*Q*b, x, t);

figure
subplot(211)

plot([t' t'],[x' y1])
title('Input and output')
legend('Input','y1')
axis([0.97 1.17 -1.2 .2])   % Only transient responses
subplot(212)
plot([t'],[y2])
title('Displacement of M2')
axis([0.97 1.17 -.5 .5])    % Only transient responses

%%
%
% Note the small difference in cost in the optimization problem from the
% 'Real' cost evaluated using norm. This is due to the regularization term.
%
fprintf('\n-----------------------------------------------\n')
fprintf('Cost according to MOSEK:           %5d \n',cost_MOSEK')
fprintf('Cost evaluated using norm:         %5d \n',cost_norm')
fprintf('H_inf bound evaluated using norm:  %5d \n', bound_norm)
fprintf('-----------------------------------------------\n')