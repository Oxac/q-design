%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%         Example 5          %%%%%%%%%%%
%%%%%%%%%%%      Mixed H_2/H_inf       %%%%%%%%%%%
%%%%%%%%%%%  Single Pole - Time Delay  %%%%%%%%%%%
%%%%%%%%%%% Written by Olle Kjellqvist %%%%%%%%%%%
%%%%%%%%%%%         2018/07/19         %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% This example solves a H_2/H_inf mixed problem for a stable, time-delayed
% single-pole system.
%
P = ss(zpk([], -1/4, 1/4, 'OutputDelay', 1));
Ms = 1.5;       % Constraint on S = 1/(1 + PC)  = 1 - PQ
Mt = 1.5;       % Constraint on T = 1 - S       = PQ
Mks = 10;       % Constraint on CS              = Q
Pnod = P;
Pnod.outputdelay = 0;
% Generate a Ritz expansion of Laguerre filters
a = 2;         % Cut-off frequency
N = 200;        % Number of elements in the basis expansion

% This is a little bit of magic to generate the state-space matrices of
% the Ritz expansion.
D = kron([-a -2*a*(-1).^(1:(N-1))],ones(N, 1));
A = full(spdiags(D, 0:N-1, N, N));
B = eye(N);
C = sqrt(2*a)*(-1).^(0:N-1);
Q = ss(A, B, C, 0);

%%
%
% We need to make sure that the delays are either output, or input delays.
%
PPQ = minreal(Pnod*Pnod*Q);
PPQ.outputdelay = 2;


%
% Construct quadratic cost
%

Cquad = PPQ.b'*lyap(PPQ.A', PPQ.C'*PPQ.C)*PPQ.B;
Clin = -2*P.b'*expm(P.A')*lyap(P.A', PPQ.A, P.C'*PPQ.C)*PPQ.b;
Cconst = P.b'*lyap(P.A', P.C'*P.C)*P.b;

% Complementary sensitivity function = T*b
T = minreal(Pnod*Q);

%
% Create grid points for H_inf constraints
%
Nw = 2000;
w = logspace(-1, 3, Nw);

T_d_fr = squeeze(freqresp(minreal(P*Q),w)).';
T_fr = squeeze(freqresp(T, w')).';      % Absolute value in frequency domain
% is equivalent without delay!

Q_fr = squeeze(freqresp(Q, w')).';

%
%   For asymptotic rejection of step disturbance
%
PPQ0 = squeeze(freqresp(PPQ, 0));
P0 = squeeze(freqresp(P, 0));

%
% Run CVX
%
cvx_clear
cvx_begin 
cvx_solver gurobi
variable b(N)

% Formulate quadratic cost
objective = b'*Cquad*b + Clin*b + Cconst
minimize objective

subject to
    abs(T_fr*b) <= Mt               
   0 <= P0 - PPQ0*b <= 0;           % Asymptotic rejection of load disturbance
    abs(1-T_d_fr*b) <= Ms
    abs(Q_fr*b) <= Mks
cvx_end

%% Present results
close all
figure(1)
subplot(211)
impulse(minreal(P - PPQ*b))
title('Impulse Disturbance')
subplot(212)
step(minreal(P-PPQ*b))
title('Step Disturbance')
figure(2)
w_dense = logspace(-4,6,10000)';
gangof4_plot_Q(P, Q*b, w_dense);
            