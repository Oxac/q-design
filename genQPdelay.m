function [Cquad, Clin, Cconst] = genQPdelay(P, D_dist, W_proc, W_sensor, rho, Q)
%%genQPdelay Generate Quadratic Problem for time-Delayed systems
%   genQPdelay(P, D_dist, W_proc, W_sensor, rho, Q) generates the quadratic
%   problem for a Q-design equivalent to the LQG-problem where P is the
%   plant, D_dist is the load-disturbance spectral-factor, W_proc is the
%   load-disturbance intensity, W_sensor is the measurement-noise 
%   intensity (assumed white). rho is the weighting factor between x and u,
%   and Q is state-space object such that Q*b, for some vector b is the
%   Ritz expansion of the Youla-parameter.
%   
%   Written by Olle Kjellqvist 2018/07/19
A_q = Q.a;
B_q = Q.b;
C_q = Q.c;
tau = P.outputdelay;
N = size(A_q,1);

PD = minreal(P*D_dist);



Ad = blkdiag(P.A, D_dist.A, P.A, A_q);
Asup = blkdiag(P.B*D_dist.C, D_dist.B*P.C, P.B*C_q);

C = blkdiag(W_proc*P.c, sqrt(rho)*W_proc*D_dist.c, W_sensor*P.c, sqrt(rho)*W_sensor*C_q);

A = Ad;
A(1:end-size(A_q,1), size(P.A,1) + 1:end) = A(1:end-size(A_q,1), size(P.A,1) + 1:end) + Asup;

Bzero = zeros(size(A,1) - size(A_q, 1), size(B_q,2));
B = [Bzero;B_q];


M_pd = lyap(PD.A', PD.c'*PD.c);
M_pd_pdpq = lyap(PD.A', A, PD.C'*C(1,:));

Cquad   = B'*lyap(A', C'*C)*B;      % Quadratic term
Clin    =  -2*PD.B'*expm(tau*PD.A')*M_pd_pdpq(:, end-N+1:end)*W_proc*B_q;  % Linear term
Cconst  = PD.B'*M_pd*PD.B*W_proc^2; % Constant term